#ifndef HENCRYPT_H
#define HENCRYPT_H
#include "Crypto.h"


class Hencrypt : public Crypto {
    public:
        Hencrypt();
        virtual ~Hencrypt();
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // HENCRYPT_H
