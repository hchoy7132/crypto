#ifndef HERNANDEZENCRYPT_H
#define HERNANDEZENCRYPT_H
#include "Crypto.h"


class HernandezEncrypt: public Crypto {
    public:
        HernandezEncrypt();
        virtual ~HernandezEncrypt();
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // HERNANDEZENCRYPT_H
