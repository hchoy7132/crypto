#ifndef TROYENCRYPTASTIC_H
#define TROYENCRYPTASTIC_H
#include "Crypto.h"


class TroyEncryptastic: public Crypto
{
    public:
        TroyEncryptastic();
        virtual ~TroyEncryptastic();
        std::string encode(std::string s);
        std::string decode(std::string s);
    protected:
    private:
};

#endif // TROYENCRYPTASTIC_H
