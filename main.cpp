#include <iostream>
#include <memory>
#include "include/TroyEncryptastic.h"
#include "include/HernandezEncrypt.h"
#include "include/Hencrypt.h"

#include "include/Crypto.h"
#include "include/CryptoImpl.h"
#include "include/CryptoChain.h"

using namespace std;

int main() {

    CryptoChain cc;

    auto_ptr<Crypto> c1(new TroyEncryptastic);
    auto_ptr<Crypto> c2(new HernandezEncrypt);
    auto_ptr<Crypto> c3(new Hencrypt);

    cc.add(c1.get());
    cc.add(c2.get());
    cc.add(c3.get());

    string input = "Hello, World!";
    string encoded = cc.encode(input);
    string decoded = cc.decode(encoded);

    cout << input << " -> " << encoded << " -> " << decoded;
}
