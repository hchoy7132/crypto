#include "../include/Hencrypt.h"
#include <string>
#include <iostream>

using namespace std;

Hencrypt::Hencrypt(){
    //ctor
}

Hencrypt::~Hencrypt(){
    //dtor
}

string Hencrypt::encode(string s){

    int length = s.length();

    try{
        for(int i = 0; i < length; i++){
            switch(s[i]){
            case 'a':
                s[i] = 'p';
                break;
            case 'e':
                s[i] = 'g';
                break;
            case 'i':
                s[i] = 't';
                break;
            case 'o':
                s[i] = 'f';
                break;
            case 'u':
                s[i] = 'b';
                break;
            case 'p':
                s[i] = 'a';
                break;
            case 'g':
                s[i] = 'e';
                break;
            case 't':
                s[i] = 'i';
                break;
            case 'f':
                s[i] = 'o';
                break;
            case 'b':
                s[i] = 'u';
                break;}
        }
    } catch(...){
        return "an error has occurred";
    }


    return s;
}

string Hencrypt::decode(string s){
    int length = s.length();

    try{
        for(int i = 0; i < length; i++){
            switch(s[i]){
            case 'p':
                s[i] = 'a';
                break;
            case 'g':
                s[i] = 'e';
                break;
            case 't':
                s[i] = 'i';
                break;
            case 'f':
                s[i] = 'o';
                break;
            case 'b':
                s[i] = 'u';
                break;
            case 'a':
                s[i] = 'p';
                break;
            case 'e':
                s[i] = 'g';
                break;
            case 'i':
                s[i] = 't';
                break;
            case 'o':
                s[i] = 'f';
                break;
            case 'u':
                s[i] = 'b';
                break;}
        }
    } catch(...){
        return "an error has occurred";
    }

    return s;
}
