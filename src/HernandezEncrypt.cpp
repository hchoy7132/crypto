#include "../include/HernandezEncrypt.h"
#include <string>
#include <iostream>
using namespace std;

HernandezEncrypt::HernandezEncrypt(){
    //ctor
}

HernandezEncrypt::~HernandezEncrypt(){
    //dtor
}

string HernandezEncrypt::encode(string s) {
    try {
        for (int i = 0; i < s.length(); i++) {
            if(s[i]=='H'){
                s[i]='9';
            }
            if(s[i]=='e'){
                s[i]='.';
            }
            if(s[i]=='l'){
                s[i]='/';
            }
            if(s[i]=='o'){
                s[i]='G';
            }
            if(i==6){
                s[i]='V';
            }
            if(i==9){
                char t = s[i];
                s[i]=s[i+2];
                s[i+2]=t;
            }
        }
    }
    catch(...){
        return "Sorry, an error occurred.";
    }
    return s;
}

string HernandezEncrypt::decode(string s) {
    try {
        for (int i = 0; i < s.length(); i++) {
            if(s[i]=='9'){
                s[i]='H';
            }
            if(s[i]=='.'){
                s[i]='e';
            }
            if(s[i]=='/'){
                s[i]='l';
            }
            if(s[i]=='G'){
                s[i]='o';
            }
            if(s[i]=='V'){
                s[i]=' ';
            }
            if(i==9){
                char t = s[i+2];
                s[i+2]=s[i];
                s[i]=t;
            }
        }
    }
    catch(...){
        return "Sorry, an error occurred.";
    }
    return s;
}
